//---------------------------------------------------------------
// File: Code136_Queue.h
// Purpose: Header file for a demonstration of a queue implemented 
//        as a linked structure.  Data type: Character
// Programming Language: C
// Author: Dr. Rick Coleman
//---------------------------------------------------------------
#ifndef CODE136_SLOTQUEUE_H
#define CODE136_SLOTQUEUE_H

#include "kernel.h"
#define MAX_SIZE    1000        // Define maximum length of the queue

typedef struct SlotQueue
{
    int head, tail;  // Declare global indices to head and tail of queue
	struct dma_ring_slot* theQueue[MAX_SIZE]; // The queue
}SlotQueue;
                 // Return true if queue is full


// List Function Prototypes
int SlotLength(SlotQueue* q);
void InitSlotQueue(SlotQueue* q);                // Initialize the queue
void ClearSlotQueue(SlotQueue* q);               // Remove all items from the queue
void SlotEnqueue(SlotQueue* q, struct dma_ring_slot* ch);            // Enter an item in the queue
struct dma_ring_slot* SlotDequeue(SlotQueue* q);                  // Remove an item from the queue
int isSlotEmpty(SlotQueue* q);                   // Return true if queue is empty
int isSlotFull();   

// Define TRUE and FALSE if they have not already been defined
#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif

#endif // End of queue header
