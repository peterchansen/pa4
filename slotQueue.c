//---------------------------------------------------------------
// File: Code130_Queue.c
// Purpose: Implementation file for a demonstration of a queue  
//      implemented as an array.    Data type: Character
// Programming Language: C
// Author: Dr. Rick Coleman
// Date: February 11, 2002
//---------------------------------------------------------------
#include "slotQueue.h"

// Declare these as static so no code outside of this source
// can access them.

int SlotLength(SlotQueue* q) {
    return q->head - q->tail;
}
//--------------------------------------------
// Function: InitQueue()
// Purpose: Initialize queue to empty.
// Returns: void
//--------------------------------------------
void InitSlotQueue(SlotQueue* q)
{
    q->head = q->tail = -1;
}

//--------------------------------------------
// Function: ClearQueue()
// Purpose: Remove all items from the queue
// Returns: void
//--------------------------------------------
void ClearSlotQueue(SlotQueue* q)
{
    q->head = q->tail = -1; // Reset indices to start over
}

//--------------------------------------------
// Function: Enqueue()
// Purpose: Enqueue an item into the queue.
// Returns: TRUE if enqueue was successful
//      or FALSE if the enqueue failed.
// Note: We let head and tail continuing 
//      increasing and use [head % MAX_SIZE] 
//      and [tail % MAX_SIZE] to get the real
//      indices.  This automatically handles
//      wrap-around when the end of the array
//      is reached.
//--------------------------------------------

void SlotEnqueue(SlotQueue* q, struct dma_ring_slot* pkt)
{
    // Check to see if the Queue is full
    if (isSlotFull(q))
    {
        printf("OVERFLOW!!!\n");
    }
    // Increment tail index
    q->tail++;
    // Add the item to the Queue
    q->theQueue[q->tail % MAX_SIZE] = pkt;
}

//--------------------------------------------
// Function: Dequeue()
// Purpose: Dequeue an item from the Queue.
// Returns: TRUE if dequeue was successful
//      or FALSE if the dequeue failed.
//--------------------------------------------
struct dma_ring_slot* SlotDequeue(SlotQueue* q)
{
    struct dma_ring_slot* pkt;

    // Check for empty Queue
        q->head++;
        pkt = q->theQueue[q->head % MAX_SIZE];     // Get character to return
        return pkt;              // Return popped character
    
}

//--------------------------------------------
// Function: isEmpty()
// Purpose: Return true if the queue is empty
// Returns: TRUE if empty, otherwise FALSE
// Note: C has no boolean data type so we use
//  the defined int values for TRUE and FALSE
//  instead.
//--------------------------------------------
int isSlotEmpty(SlotQueue* q)
{
    return (q->head == q->tail);
}

//--------------------------------------------
// Function: isFull()
// Purpose: Return true if the queue is full.
// Returns: TRUE if full, otherwise FALSE
// Note: C has no boolean data type so we use
//  the defined int values for TRUE and FALSE
//  instead.
//--------------------------------------------
int isSlotFull(SlotQueue* q)
{
    // Queue is full if tail has wrapped around
    //  to location of the head.  See note in
    //  Enqueue() function.
    return ((q->tail - MAX_SIZE) == q->head);
}
