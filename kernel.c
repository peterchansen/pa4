#include "kernel.h"
#include "queue.h"
#include "hashmap.h"
#include "slotQueue.h"
#include "commandQueue.h"
struct bootparams *bootparams;

int debug = 1; // change to 0 to stop seeing so many messages

int PRINTSTATS = 0;
void shutdown() {
  puts("Shutting down...");
  // this is really the "wait" instruction, which gcc doesn't seem to know about
  __asm__ __volatile__ ( ".word 0x42000020\n\t");
  while (1);
}
unsigned short endian_switch_short (unsigned short s) {
    
    unsigned char c1, c2;
    c1 = s & 255;
    c2 = (s >> 8) & 255;

    return (c1 << 8) + c2;
}

//reverses byte order of int (4 byte) values. Used to reverse data_big_endian, which is an unsigned int value.
unsigned int endian_switch_int (unsigned int i) {
    
    unsigned char c1, c2, c3, c4;
    c1 = i & 255;
    c2 = (i >> 8) & 255;
    c3 = (i >> 16) & 255;
    c4 = (i >> 24) & 255;

    return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
}

/* Trap handling.
 *
 * Only core 0 will get interrupts, but any core can get an exception (or
 * syscall).  So we allocate enough space for every core i to have its own place
 * to store register state (trap_save_state[i]), its own stack to use in the
 * interrupt handler (trap_stack_top[i]), and a valid $gp to use during
 * interrupt handling (trap_gp[i]).
 */

struct mips_core_data *trap_save_state[MAX_CORES]; /* one save-state per core */
void *trap_stack_top[MAX_CORES]; /* one trap stack per core */
unsigned int trap_gp[MAX_CORES]; /* one trap $gp per core */

void trap_init()
{
  int id = current_cpu_id();

  /* trap should use the same $gp as the current code */
  trap_gp[id] = current_cpu_gp();

  /* trap should use a fresh stack */
  void *bottom = alloc_pages(4);
  trap_stack_top[id] = bottom + 4*PAGE_SIZE - 4;

  /* put the trap save-state at the top of the corresponding trap stack */
  trap_stack_top[id] -= sizeof(struct mips_core_data);
  trap_save_state[id] = trap_stack_top[id];

  /* it is now safe to take interrupts on this core */
  intr_restore(1);
}

void interrupt_handler(int cause)
{
  // note: interrupts will only happen on core 0
  // diagnose the source(s) of the interrupt trap
  int pending_interrupts = (cause >> 8) & 0xff;
  int unhandled_interrupts = pending_interrupts;

  if (pending_interrupts & (1 << INTR_KEYBOARD)) {
    if (debug) printf("interrupt_handler: got a keyboard interrupt, handling it\n");
    keyboard_trap(&PRINTSTATS);
    unhandled_interrupts &= ~(1 << INTR_KEYBOARD);
  }

  if (pending_interrupts & (1 << INTR_TIMER)) {
    printf("interrupt_handler: got a spurious timer interrupt, ignoring it and hoping it doesn't happen again\n");
    unhandled_interrupts &= ~(1 << INTR_TIMER);
  }

  if (unhandled_interrupts != 0) {
    printf("got interrupt_handler: one or more other interrupts (0x%08x)...\n", unhandled_interrupts);
  }

}

void trap_handler(struct mips_core_data *state, unsigned int status, unsigned int cause)
{
  if (debug) printf("trap_handler: status=0x%08x cause=0x%08x on core %d\n", status, cause, current_cpu_id());
  // diagnose the cause of the trap
  int ecode = (cause & 0x7c) >> 2;
  switch (ecode) {
    case ECODE_INT:   /* external interrupt */
      interrupt_handler(cause);
      return; /* this is the only exception we currently handle; all others cause a shutdown() */
    case ECODE_MOD:   /* attempt to write to a non-writable page */
      printf("trap_handler: some code is trying to write to a non-writable page!\n");
      break;
    case ECODE_TLBL:    /* page fault during load or instruction fetch */
    case ECODE_TLBS:    /* page fault during store */
      printf("trap_handler: some code is trying to access a bad virtual address!\n");
      break;
    case ECODE_ADDRL:   /* unaligned address during load or instruction fetch */
    case ECODE_ADDRS:   /* unaligned address during store */
      printf("trap_handler: some code is trying to access a mis-aligned address!\n");
      break;
    case ECODE_IBUS:    /* instruction fetch bus error */
      printf("trap_handler: some code is trying to execute non-RAM physical addresses!\n");
      break;
    case ECODE_DBUS:    /* data load/store bus error */
      printf("trap_handler: some code is read or write physical address that can't be!\n");
      break;
    case ECODE_SYSCALL:   /* system call */
      printf("trap_handler: who is doing a syscall? not in this project...\n");
      break;
    case ECODE_BKPT:    /* breakpoint */
      printf("trap_handler: reached breakpoint, or maybe did a divide by zero!\n");
      break;
    case ECODE_RI:    /* reserved opcode */
      printf("trap_handler: trying to execute something that isn't a valid instruction!\n");
      break;
    case ECODE_OVF:   /* arithmetic overflow */
      printf("trap_handler: some code had an arithmetic overflow!\n");
      break;
    case ECODE_NOEX:    /* attempt to execute to a non-executable page */
      printf("trap_handler: some code attempted to execute a non-executable virtual address!\n");
      break;
    default:
      printf("trap_handler: unknown error code 0x%x\n", ecode);
      break;
  }
  shutdown();
}
//djb2 hash function
unsigned long djb2(unsigned char *pkt, int n)
{
  unsigned long hash = 5381;
  int i = 0;
  while (i < n-8) {
    hash = ((hash << 5) + hash) + pkt[i++];
    hash = ((hash << 5) + hash) + pkt[i++];
    hash = ((hash << 5) + hash) + pkt[i++];
    hash = ((hash << 5) + hash) + pkt[i++];
    hash = ((hash << 5) + hash) + pkt[i++];
    hash = ((hash << 5) + hash) + pkt[i++];
    hash = ((hash << 5) + hash) + pkt[i++];
    hash = ((hash << 5) + hash) + pkt[i++];
  }
  while (i < n)
    hash = ((hash << 5) + hash) + pkt[i++];
  return hash;
}
hashmap* spammerHash;
int spammerHashLock = 0;
hashmap* portsHash;
int portsHashLock = 0;
hashmap* evilHash;
int evilHashLock = 0;
hashmap* evilHash2;
int evilHashLock2 = 0;
SlotQueue* stage1Queue;
int stage1Lock = 0;
SlotQueue* stage2Queue;
int stage2Lock = 0;
SlotQueue* stage3Queue;
int stage3Lock = 0;
SlotQueue* stage4Queue;
int stage4Lock = 0;
SlotQueue* stage4aQueue;
int stage4aLock = 0;
SlotQueue* stage5Queue;
int stage5Lock = 0;
SlotQueue* commandQueue;
int commandQueueLock = 0;
SlotQueue* spamQueue;
int spamQueueLock = 0;
SlotQueue* portQueue;
int portQueueLock = 0;
unsigned int numPackets = 0;
int numBytes = 0;
int numSpam = 0;
int numVuln = 0;
int numEvil = 0;
int startCycles = 0;
int evilKey = 0;
/* kernel entry point called at the end of the boot sequence */
void __boot() {
  //Queue* stage1Queue;
  if (current_cpu_id() == 0) {
    /* core 0 boots first, and does all of the initialization */

    // boot parameters are on physical page 0
    bootparams = physical_to_virtual(0x00000000);

    // initialize console early, so output works
    console_init();

    // output should now work
    printf("Welcome to my kernel!\n");
    printf("Running on a %d-way multi-core machine\n", current_cpu_exists());

    // initialize memory allocators
    mem_init();

    // prepare to handle interrupts, exceptions, etc.
    trap_init();

    // initialize keyboard late, since it isn't really used by anything else
    keyboard_init();


    // see which cores are already on
    for (int i = 0; i < 32; i++)
      printf("CPU[%d] is %s\n", i, (current_cpu_enable() & (1<<i)) ? "on" : "off");

    // turn on all other cores
    set_cpu_enable(0xFFFFFFFF);

    // see which cores got turned on
    busy_wait(0.1);
    for (int i = 0; i < 32; i++)
      printf("CPU[%d] is %s\n", i, (current_cpu_enable() & (1<<i)) ? "on" : "off");


    // initialize queues so the links between cores will only have one location in memory
    stage1Queue = (SlotQueue*)malloc(sizeof(SlotQueue));
    InitSlotQueue(stage1Queue);
    stage2Queue = (SlotQueue*)malloc(sizeof(SlotQueue));
    InitSlotQueue(stage2Queue);
    stage3Queue = (SlotQueue*)malloc(sizeof(SlotQueue));
    InitSlotQueue(stage3Queue);
    stage4Queue = (SlotQueue*)malloc(sizeof(SlotQueue));
    InitSlotQueue(stage4Queue);
    stage4aQueue = (SlotQueue*)malloc(sizeof(SlotQueue));
    InitSlotQueue(stage4aQueue);
    commandQueue = (SlotQueue*)malloc(sizeof(SlotQueue));
    InitSlotQueue(commandQueue);
    spamQueue = (SlotQueue*)malloc(sizeof(Queue));
    InitSlotQueue(spamQueue);
    portQueue = (SlotQueue*)malloc(sizeof(Queue));
    InitSlotQueue(portQueue);
    // Initialize hashmaps to store statistics that will be tracked
    spammerHash = hashmapCreate(199);
    portsHash = hashmapCreate(199);
    evilHash = hashmapCreate(199);
    evilHash2 = hashmapCreate(199);
    // printf("%d", *stage1Queue);
    network_init();
    network_set_interrupts(0);
    //network_start_receive();
    
  } else {
    /* remaining cores boot after core 0 turns them on */


  }

  printf("Core %d of %d is alive!\n", current_cpu_id(), current_cpu_exists());

  busy_wait(current_cpu_id() * 0.1); // wait a while so messages from different cores don't get so mixed up
  // int size = 64 * 1024 * 4;
  // printf("about to do calloc(%d, 1)\n", size);
  // unsigned int t0  = current_cpu_cycles();
  // calloc(size, 1);
  // unsigned int t1  = current_cpu_cycles();
  // printf("DONE (%u cycles)!\n", t1 - t0);

  // while (1) ;

  // for (int i = 1; i < 30; i++) {
  //   int size = 1 << i;
  //   printf("about to do calloc(%d, 1)\n", size);
  //   calloc(size, 1);
  // }



  while (1) {
    // printf("Core %d is still running...\n", current_cpu_id());
    if (current_cpu_id() == 0) 
    {
      startCycles = current_cpu_cycles();
      network_poll(stage1Queue, &stage1Lock, stage4Queue, &stage4Lock, stage4aQueue, &stage4aLock);
    }
        /*if (current_cpu_id() == 1) {
      int i = 0;
      while(!stage1Queue) {
        i++;
      }
      while(1) {
        printf("here\n");
        if (!isEmpty(stage1Queue))
        {
        printf("here\n");
          struct dma_ring_slot* packet = Dequeue(stage1Queue);
          struct honeypot_command_packet* pkt = (struct honeypot_command_packet*)physical_to_virtual(packet->dma_base);
          printf("%d\n", pkt->secret_big_endian);
        }
      }
    }*/
      if (current_cpu_id() == 1) {
        while(!stage1Queue) {}
        while(1) {
          mutex_lock(&stage1Lock);
          if (!isSlotEmpty(stage1Queue))
          {
            struct dma_ring_slot* packet = SlotDequeue(stage1Queue);
            mutex_unlock(&stage1Lock); 
            numPackets++;
            numBytes+=packet->dma_len;
            struct honeypot_command_packet* pkt = (struct honeypot_command_packet*)physical_to_virtual(packet->dma_base);
            // printf("packet\n");
            if ((pkt->secret_big_endian == 4148))
            {

            unsigned short cmd = endian_switch_short(pkt->cmd_big_endian);
            unsigned int data = endian_switch_int(pkt->data_big_endian);
              if (cmd == HONEYPOT_DEL_SPAMMER)
              {
                  hashmapRemove(spammerHash, data);
              } else if (cmd == HONEYPOT_DEL_VULNERABLE)
              {
                  hashmapRemove(portsHash, data);
              } else if (cmd == HONEYPOT_DEL_EVIL) {
                  hashmapRemove(evilHash, data);
              } else {
              mutex_lock(&commandQueueLock);
              SlotEnqueue(commandQueue, packet);
              mutex_unlock(&commandQueueLock);
              // printf("enqueued command\n");
            }
            } else {
            mutex_lock(&stage2Lock);
            SlotEnqueue(stage2Queue, packet);
            mutex_unlock(&stage2Lock);
          }

            // packet++;
            // printf("%d\n", packet->dma_len);
            // // struct packet_header header = pkt->headers;
          } else {
            mutex_unlock(&stage1Lock); 

          }
        }
      }
      if (current_cpu_id() == 2) 
      {
        while(!stage2Queue) {}
            while(1) {
                 mutex_lock(&stage2Lock);
              if (!isSlotEmpty(stage2Queue))
              {
                 struct dma_ring_slot* packet = SlotDequeue(stage2Queue);
                 mutex_unlock(&stage2Lock);
                 struct packet_header* pkt = (struct packet_header*)physical_to_virtual(packet->dma_base);
                 mutex_lock(&spammerHashLock);
                 if ( hashmapGet(spammerHash, endian_switch_int( pkt->ip_source_address_big_endian)) == 1)
                 {
                  // printf("found spammer\n");
                   // mutex_lock(&spamQueueLock);
                   // SlotEnqueue(spamQueue, packet);
                   // mutex_unlock(&spamQueueLock);
                   numSpam++;
                 }
                 mutex_unlock(&spammerHashLock);
                 mutex_lock(&stage3Lock);
                 SlotEnqueue(stage3Queue, packet);
                 mutex_unlock(&stage3Lock);
              } else {
                 mutex_unlock(&stage2Lock);
              }
            }
      }
      if (current_cpu_id() == 3) {
        
        while(!stage3Queue) {}
            while(1) {
                 mutex_lock(&stage3Lock);
              if (!isSlotEmpty(stage3Queue))
              {
                 struct dma_ring_slot* packet = SlotDequeue(stage3Queue);
                 mutex_unlock(&stage3Lock);
                 struct packet_header* pkt = (struct packet_header*)physical_to_virtual(packet->dma_base);
                 mutex_lock(&portsHashLock);
                 if (hashmapGet(portsHash, endian_switch_short(pkt->udp_dest_port_big_endian)) ==1 )
                 {
                  // printf("found port\n");
                   // mutex_lock(&portQueueLock);
                   // SlotEnqueue(portQueue, packet);
                   // mutex_unlock(&portQueueLock);
                   numVuln++;
                 }
                 mutex_unlock(&portsHashLock);
              } else {
                 mutex_unlock(&stage3Lock);
              }
            }
      }
      if (current_cpu_id() == 4) 
      {
        while(!commandQueue) {}
        while(1) {
            mutex_lock(&commandQueueLock);
          if (!isSlotEmpty(commandQueue))
          {
            struct dma_ring_slot* packet = SlotDequeue(commandQueue);
            mutex_unlock(&commandQueueLock); 
            struct honeypot_command_packet* pkt = (struct honeypot_command_packet*)physical_to_virtual(packet->dma_base);
            unsigned short cmd = endian_switch_short(pkt->cmd_big_endian);
            unsigned int data = endian_switch_int(pkt->data_big_endian);
            if (cmd == HONEYPOT_ADD_SPAMMER)
            {
              mutex_lock(&spammerHashLock);
               hashmapInsert(spammerHash, 1, data);
               mutex_unlock(&spammerHashLock);
            } if (cmd == HONEYPOT_ADD_EVIL) {
              mutex_lock(&evilHashLock);
               hashmapInsert(evilHash, 1, data);
               mutex_unlock(&evilHashLock);
              mutex_lock(&evilHashLock2);
               hashmapInsert(evilHash2, 1, data);
               mutex_unlock(&evilHashLock2);
            } if (cmd == HONEYPOT_ADD_VULNERABLE) {
              mutex_lock(&portsHashLock);
               hashmapInsert(portsHash, 1, data);
               mutex_unlock(&portsHashLock);
            } if (cmd == HONEYPOT_DEL_SPAMMER)
            {
              mutex_lock(&spammerHashLock);
              hashmapRemove(spammerHash, data);
              mutex_unlock(&spammerHashLock);
            } if (cmd == HONEYPOT_DEL_EVIL)
            {
              mutex_lock(&evilHashLock);
              hashmapRemove(evilHash, data);
              mutex_unlock(&evilHashLock);
              mutex_lock(&evilHashLock2);
              hashmapRemove(evilHash2, data);
              mutex_unlock(&evilHashLock2);
            } if (cmd == HONEYPOT_DEL_VULNERABLE)
            {
              mutex_lock(&portsHashLock);
              hashmapRemove(portsHash, data);
              mutex_unlock(&portsHashLock);
            } if (cmd == HONEYPOT_PRINT)
            {
              PRINTSTATS = 1;
            }
          } else {
            mutex_unlock(&commandQueueLock); 
          }
        }
      }
      if (current_cpu_id() > 4 && current_cpu_id() < 18) {
         while(!stage4Queue) {}

        while(1) {
          while(1) {
            mutex_lock(&stage4Lock);
          if (!isSlotEmpty(stage4Queue))
          {
            struct dma_ring_slot* slot = SlotDequeue(stage4Queue);
            mutex_unlock(&stage4Lock);

            unsigned int pkt_len = slot->dma_len;
            struct honeypot_command_packet* pkt = (struct honeypot_command_packet*)physical_to_virtual(slot->dma_base);
            unsigned long pkt_hash = djb2((unsigned char*) pkt, pkt_len);

            mutex_lock(&evilHashLock);
            if (hashmapGet(evilHash, pkt_hash) == 1) {
              mutex_lock(&evilKey);
                numEvil++;
                mutex_unlock(&evilKey);
            }
            mutex_unlock(&evilHashLock);

            //free memory
          } else {
            mutex_unlock(&stage4Lock);

          }
        }
      }
    }
if (current_cpu_id() > 18 && current_cpu_id() < 31) {
         while(!stage4aQueue) {}

        while(1) {
          while(1) {
            mutex_lock(&stage4aLock);
          if (!isSlotEmpty(stage4aQueue))
          {
            struct dma_ring_slot* slot = SlotDequeue(stage4aQueue);
            mutex_unlock(&stage4aLock);

            unsigned int pkt_len = slot->dma_len;
            struct honeypot_command_packet* pkt = (struct honeypot_command_packet*)physical_to_virtual(slot->dma_base);
            unsigned long pkt_hash = djb2((unsigned char*) pkt, pkt_len);

            mutex_lock(&evilHashLock2);
            if (hashmapGet(evilHash, pkt_hash) != 0) {
              mutex_lock(&evilKey);
                numEvil++;
                mutex_unlock(&evilKey);
            }
            mutex_unlock(&evilHashLock2);

            //free memory
          } else {
            mutex_unlock(&stage4aLock);

          }
        }
        }
      }
      if (current_cpu_id() == 31)
      {
          while(1) {
            if (PRINTSTATS)
            {
              double seconds = ((double)current_cpu_cycles() - (double)startCycles)/1000000;
              double bpsdouble = (double)numBytes/seconds * 8;
              int bps = (int)bpsdouble;
              printf("Number of Packets: %d \n Number of Bytes: %d \n Bits/Second: %d \n Number of spam packets: %d \n Number of vulnerable port packets: %d \n Number of evil packets: %d \n", numPackets, numBytes , bps, numSpam, numVuln, numEvil);
              PRINTSTATS = 0;
            }
          }
      }
  }

  shutdown();
}
