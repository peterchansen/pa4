
#ifndef HASHMAP_H_INCLUDED
#define HASHMAP_H_INCLUDED
#include "kernel.h"
/** Hashmap structure (forward declaration) */
struct s_hashmap;
typedef struct s_hashmap hashmap;

/** Creates a new hashmap near the given size. */
extern hashmap* hashmapCreate(int startsize);

/** Inserts a new element into the hashmap. */
extern void hashmapInsert(hashmap*, int data, unsigned int key);

/** Removes the storage for the element of the key and returns the element. */
extern int hashmapRemove(hashmap*, unsigned int key);

/** Returns the element for the key. */
extern int hashmapGet(hashmap*, unsigned int key);

/** Returns the number of saved elements. */
extern int hashmapCount(hashmap*);

/** Removes the hashmap structure. */
extern void hashmapDelete(hashmap*);

#endif
