#ifndef LOCK_H_
#define LOCK_H_

//if m == 0, lock is free; if m == 1, lock is taken
//if lock is not free, spin wait until it is, then take it
void mutex_lock(int *m);

//frees the lock
void mutex_unlock(int *m);

#endif
