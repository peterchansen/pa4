//---------------------------------------------------------------
// File: Code136_Queue.c
// Purpose: Implementation file for a demonstration of a queue  
//        implemented as a linked structure.    Data type: Character
// Programming Language: C
// Author: Dr. Rick Coleman
// Date: February 11, 2002
//---------------------------------------------------------------
#include "commandQueue.h"
#define NULL 0

// Declare these as static so no code outside of this source
// can access them./ Declare global pointers to head and tail of queue

//--------------------------------------------
// Function: InitQueue()
// Purpose: Initialize queue to empty.
// Returns: void
//--------------------------------------------
void InitCommandQueue(CommandQueue* q)
{
    q->head = NULL;
    q->tail = NULL;
}

//--------------------------------------------
// Function: ClearQueue()
// Purpose: Remove all items from the queue
// Returns: void
//--------------------------------------------
void CommandClearQueue(CommandQueue* q)
{
    CommandQNode *temp;
    while(q->head != NULL)
    {
        temp = q->head;
        q->head = q->head->next;
        free(temp);
    }

    q->head = q->tail = NULL; // Reset indices to start over
}

//--------------------------------------------
// Function: Enqueue()
// Purpose: Enqueue an item into the queue.
// Returns: TRUE if enqueue was successful
//        or FALSE if the enqueue failed.
//--------------------------------------------
void CommandEnqueue(CommandQueue* q, struct honeypot_command_packet* pkt, int key)
{
    CommandQNode *temp;
    // temp++;

    // Create new node for the queue
    mutex_lock(&key);
    temp = (CommandQNode *)malloc(sizeof(QNode));
    mutex_unlock(&key);
    temp->pkt = pkt;
    temp->next = NULL;

    // Check for inserting first in the queue
    if(q->head == NULL)
        q->head = q->tail = temp;
    else
    {
        q->tail->next = temp; // Insert into the queue
        q->tail = temp;       // Set tail to new last node
    }

}

//--------------------------------------------
// Function: Dequeue()
// Purpose: Dequeue an item from the Queue.
// Returns: TRUE if dequeue was successful
//        or FALSE if the dequeue failed.
//--------------------------------------------
/*
ABSOLUTELY MUST CHECK IF QUEUE IS EMPTY BEFORE RUNNING DEQUEUE
*/
struct honeypot_command_packet* CommandDequeue(CommandQueue* q)
{
    struct honeypot_command_packet* pkt;
    CommandQNode *temp;

    pkt = q->head->pkt;        // Get character to return
    temp = q->head;
    q->head = q->head->next;    // Advance head pointer
    free(temp);            // Free old head
    // Check to see if queue is empty
    if(isCommandEmpty(q))
    {
        q->head = q->tail = NULL;        // Reset everything to empty queue
    }
    return pkt;                // Return popped character
}

//--------------------------------------------
// Function: isEmpty()
// Purpose: Return true if the queue is empty
// Returns: TRUE if empty, otherwise FALSE
// Note: C has no boolean data type so we use
//    the defined int values for TRUE and FALSE
//    instead.
//--------------------------------------------
int isCommandEmpty(CommandQueue* q)
{
    return (q->head == NULL);
}

//--------------------------------------------
// Function: isFull()
// Purpose: Return true if the queue is full.
// Returns: TRUE if full, otherwise FALSE
//--------------------------------------------
int isCommandFull()
{
    return FALSE;
}
