#ifndef NETWORK_H_
#define NETWORK_H_
#include "kernel.h"
#include "queue.h"
#include "slotQueue.h"
struct SlotQueue;

// Initializes the network driver, allocating the space for the ring buffer.
void network_init();

// Starts receiving packets!
void network_start_receive();

// If opt != 0, enables interrupts when a new packet arrives.
// If opt == 0, disables interrupts.
void network_set_interrupts(int opt);

// Continually polls for data on the ring buffer. Loops forever!
void network_poll(struct SlotQueue* q, int* queuelock, struct SlotQueue* queue2, int* lock2, struct SlotQueue* stage4Queue, int* stage4Lock);

// Called when a network interrupt occurs.
void network_trap();

#endif
