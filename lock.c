//SOURCE: Synchronization lecture notes slide 37
//used around prints, memory allocation, and to synchronize shared datastructures

void mutex_lock(int *m) {
    asm volatile (
        ".set mips2\n"
        "test_and_set:\n"
        "li $8, 1\n"
        "ll $9, 0($4)\n"
        "bnez $9, test_and_set\n"
        "sll $0, $0, 0\n"
        "sc $8, 0($4)\n"
        "beqz $8, test_and_set\n"
        "sll $0, $0, 0\n"
    );
}

void mutex_unlock(int *m) {
    *m = 0;
}
