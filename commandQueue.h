//---------------------------------------------------------------
// File: Code136_Queue.h
// Purpose: Header file for a demonstration of a queue implemented 
//        as a linked structure.  Data type: Character
// Programming Language: C
// Author: Dr. Rick Coleman
//---------------------------------------------------------------
#ifndef CODECOMMAND136_QUEUE_H
#define CODECOMMAND136_QUEUE_H

#include "kernel.h"

typedef struct CommandQNodeType
{
    struct honeypot_command_packet* pkt;
    struct CommandQNodeType *next;
}CommandQNode;

typedef struct CommandQueue
{
    CommandQNode *head;
    CommandQNode *tail;
}CommandQueue;
// List Function Prototypes
void InitCommandQueue(CommandQueue* q);                // Initialize the queue
void ClearCommandQueue(CommandQueue* q);               // Remove all items from the queue
void CommandEnqueue(CommandQueue* q, struct honeypot_command_packet* pkt, int key);            // Enter an item in the queue
struct honeypot_command_packet* CommandDequeue(CommandQueue* q);                  // Remove an item from the queue
int isCommandEmpty(CommandQueue* q);                   // Return true if queue is empty
int isCommandFull();                    // Return true if queue is full

// Define TRUE and FALSE if they have not already been defined
#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif

#endif // End of queue header
