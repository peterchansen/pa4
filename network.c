#include "kernel.h"
//#include "queue.h"
// Network device driver.

#define RING_SIZE 16 //NET_MAX_RING_CAPACITY 16
#define BUFFER_SIZE 4000 //NET_MAXPKT 4000
#define NULL 0

// a pointer to the memory-mapped I/O region for the network
volatile struct dev_net *dev_net;
// queue that will hold output of ring buffer
struct dma_ring_slot* rings;
int ringsIndex = 0;
void network_init() {
	/* Find out where I/O region is in memory. */
	for (int i = 0; i < 16; i++) {
		if (bootparams->devtable[i].type == DEV_TYPE_NETWORK) {
			puts("Detected network device...");
			// find a virtual address that maps to this I/O region
			dev_net = physical_to_virtual(bootparams->devtable[i].start);
			break;
		}
	}
	//allocate the ring buffer for receiving packets. Ring buffer is an array of struct dma_ring_slot.
	struct dma_ring_slot* ring = (struct dma_ring_slot*) malloc(sizeof(struct dma_ring_slot) * RING_SIZE);
	rings = (struct dma_ring_slot*) malloc(sizeof(struct dma_ring_slot) * 1430000);
	dev_net->rx_base = virtual_to_physical(ring); //rx_base is set to the physical address of the start of ring buffer
	dev_net->rx_capacity = RING_SIZE; //rx_capacity, the number of slots in the ring, is 16
	//ring starts empty => rx_head = rx_tail = 0
	dev_net->rx_head = 0;
	dev_net->rx_tail = 0;

	//for every ring slot within ring buffer, allocate a buffer of size BUFFER_SIZE
	for (int i = 0; i < RING_SIZE; i++) {
		void* space = malloc(BUFFER_SIZE);
		ring[i].dma_base = virtual_to_physical(space); //physical address of buffer
		ring[i].dma_len = BUFFER_SIZE;
	}

	puts("...network driver is ready.");

	dev_net->cmd = NET_SET_POWER; //1 to data turns on card
	dev_net->data = 1;
}

void network_start_receive() {

}

// If opt != 0, enables interrupts when a new packet arrives.
// If opt == 0, disables interrupts.
void network_set_interrupts(int opt) {

}
// continually polls for data on the ring buffer.
void network_poll(SlotQueue* q, int* queuelock, SlotQueue* queue2, int* key2, SlotQueue* queue3, int* key3) {
	dev_net->cmd = NET_SET_RECEIVE;
	dev_net->data = 1;
	int i = 0;
	while(1) {
		if (dev_net->rx_head != dev_net->rx_tail)
		{
			struct dma_ring_slot* ring_buffer_base = physical_to_virtual(dev_net->rx_base); //get virtual address of dev_net->rx_base
			int index = (dev_net->rx_tail)%(dev_net->rx_capacity); //the driver should read the slot at index (rx_tail mod rx_capacity) to locate full buffer
			(rings + ringsIndex)->dma_base = (ring_buffer_base + index)->dma_base;
			(rings + ringsIndex)->dma_len = (ring_buffer_base + index)->dma_len;
   			mutex_lock(queuelock);
            // printf("%d\n", (ring_buffer_base + index)->dma_len);
			SlotEnqueue(q, rings + ringsIndex); //full buffer containing packet is put in the queue
    		mutex_unlock(queuelock);
            if (i%2==0)
            {
            mutex_lock(key2);
            SlotEnqueue(queue2, rings + ringsIndex);
            mutex_unlock(key2);
            } else {
            mutex_lock(key3);
            SlotEnqueue(queue3, rings + ringsIndex);
            mutex_unlock(key3);

            }
            i++;
			// printf("%d\n", (ring_buffer_base + index)->dma_len);
			// printf("enqueued packet\n");
			ringsIndex = (ringsIndex + 1)%1430000;
			// ring_buffer_base[index].dma_base = virtual_to_physical(rings + ringsIndex); //replaces full buffer in (ring_buffer_base + index) with an empty buffer. dma_base is a physical address.
			ring_buffer_base[index].dma_len = BUFFER_SIZE; //reset length field to avoid truncation errors
			dev_net->rx_tail++; //increment tail after replacement
		}
	}
}
//Called when a network interrupt occurs.
void network_trap() {
	//
}
