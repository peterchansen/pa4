//---------------------------------------------------------------
// File: Code136_Queue.h
// Purpose: Header file for a demonstration of a queue implemented 
//        as a linked structure.  Data type: Character
// Programming Language: C
// Author: Dr. Rick Coleman
//---------------------------------------------------------------
#ifndef CODE136_QUEUE_H
#define CODE136_QUEUE_H

#include "kernel.h"

typedef struct QNodeType
{
    struct packet_header* headers;
    struct QNodeType *next;
}QNode;

typedef struct Queue
{
    QNode *head;
    QNode *tail;
}Queue;
// List Function Prototypes
void InitQueue(Queue* q);                // Initialize the queue
void ClearQueue(Queue* q);               // Remove all items from the queue
void Enqueue(Queue* q, struct packet_header* headers, int key);            // Enter an item in the queue
struct packet_header* Dequeue(Queue* q);                  // Remove an item from the queue
int isEmpty(Queue* q);                   // Return true if queue is empty
int isFull();                    // Return true if queue is full

// Define TRUE and FALSE if they have not already been defined
#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif

#endif // End of queue header
